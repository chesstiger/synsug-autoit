Syntactic Sugar for AutoIt3
===========================

Mit dieser Software ist es m�glich, gewisse syntaktische Spielereien
(= Syntactic Sugar), die teilweise aus anderen Programmiersprachen
(C, Java...) bekannt sind, in AutoIt zu benutzen. Technisch gesehen
arbeitet *synsug.exe* wie ein Preprocessor, d.h. das auszuf�hrende
Skript wird an das Programm �bergeben, welches dann die oben erw�hnten
Spielereien durch validen (aber umst�ndlicher zu lesenden) AutoIt-Code
ersetzt und dann an AutoIt3.exe �bergibt.

Nachdem das Installationsprogramm ausgef�hrt worden ist, kann in SciTE
jedes Skript �ber SynSug ausgef�hrt oder kompiliert werden. Die
Tastenkombinationen lauten ``Ctrl+Shift+F5`` f�r *Execute* und 
``Ctrl+Shift+F7`` f�r *Compile*.

**ACHTUNG!** Damit die SciTE-Integration reibungslos funktioniert, muss
SciTE4AutoIt installiert sein!

Benutzung
=========

Damit ein Skript �berhaupt mit Syntactic Sugar ausgef�hrt werden kann,
**muss** es die Zeile ``#SynSug_Enable=yes`` zwingend enthalten,
vorzugsweise am Skriptanfang.

Folgende Schreibweisen werden neu eingef�hrt:

1. Post- und Pr�fix-Operatoren
------------------------------
```
$i = 10
While $i--
	ConsoleWrite($i & @CRLF)
WEnd
```

2. Modulo-Operator
------------------
```
For $i = 1 To 10
	If $i % 2 Then
		ConsoleWrite("Ungerade" & @CRLF)
	Else
		ConsoleWrite("Gerade" & @CRLF)
	EndIf
Next
```

3. Bit-Operatoren
-----------------
```
$i = 10
$i = $i << 1 ;$i = 20
ConsoleWrite($i & @CRLF)
$i = $i >> 2 ;$i = 5
ConsoleWrite($i & @CRLF)
$i = ~$i ;$i = -6
ConsoleWrite($i & @CRLF)
```

4. Ausdrucksorientierte For-Schleife
------------------------------------
Beachte: Im Schleifenkopf werden keine Semikolons verwendet,
nur Doppelpunkte!
```
For ($i = 0: $i < 10: $i++)
	ConsoleWrite($i & @CRLF)
Next
```

5. Referenz-For-Schleife
------------------------------------
```
For &$i In $aiArray
	$i *= 2
Next
```
ODER
```
For ByRef $i In $aiArray
	$i *= 2
Next
```

6. Anonymes Array als R�ckgabewert
----------------------------------
```
For $i = 0 To 3
	ConsoleWrite(getArray()[$i] & @CRLF)
Next

Func getArray()
	Return ["a", "b", "c", "d"]
EndFunc
```

7. Angabe von Ganzzahl-Literalen in Bin�rnotation
-------------------------------------------------
```
ConsoleWrite(0b100 & @CRLF)
```

8. Escape-Sequenzen in String-Literalen
---------------------------------------
F�r diese Funktion muss im Skript die Zeile ``#SynSug_EscSeq=yes``
enthalten sein.
```
#SynSug_EscSeq=yes
ConsoleWrite("Erste Zeile\nZweite Zeile\n\tDritte Zeile, einger�ckt.\n")
```

9. printf-Funktion
------------------
F�r diese Funktion muss im Skript die Zeile ``#SynSug_printf=yes``
enthalten sein.
```
#SynSug_printf=yes
printf("Hello, %s!\n", "World")
```

10. Assoziative Arrays
----------------------
Es wird das ``Scripting.Dictionary``-Objekt genutzt.
```
Local Dict $oEmpty
Local Dict $oDict = ["abc" => "def", "xyz" => "qwertz"]
$oDict["new"] = "Mein Text"
ConsoleWrite(UBound($oDict) & @CRLF)
```

11. Strukturen
--------------
```
BeginStruct point_t
	int x
	int y
EndStruct

Local Struct point_t $tPointA, $tPointB
$tPointA.x = 1
$tPointA.y = 2
$tPointB.x = 6
$tPointB.y = 3

ConsoleWrite("Distance: " & getDist($tPointA, $tPointB) & @CRLF)

Func getDist($tA, $tB)
	Return Sqrt(($tA.x - $tB.x)^2 + ($tA.y - $tB.y)^2)
EndFunc
```