#include <Array.au3>
$sSynSugVer = "1.3"

;~ $sInFile = "test.au3"
$sSwitch = $CmdLine[1]
$sAu3Path = $CmdLine[2]
$sInFile = $CmdLine[3]
$sOutFile = StringRegExpReplace($sInFile, "(.+?)\.au3", "\1_synsug.au3")

$sFileContent = FileRead($sInFile)
;~ If @error Then Exit ConsoleWrite("Script '" & $sFileContent & "' not found!!!" & @CRLF)

#Region Inline Settings
ConsoleWrite(StringFormat("- = Syntactic Sugar v%s (c) %s chesstiger = -\n", $sSynSugVer, @YEAR))
$bUseSynSug = ReadDirective($sFileContent, "SynSug_Enable", "no") = "yes" ? True : False
$bUseEscSeq = ReadDirective($sFileContent, "SynSug_EscSeq", "no") = "yes" ? True : False
$bUsePrintf = ReadDirective($sFileContent, "SynSug_printf", "no") = "yes" ? True : False
$bUseConsoleTracing = ReadDirective($sFileContent, "SynSug_ConsoleTracing", "no") = "yes" ? True : False
$bUseWindowTracing  = ReadDirective($sFileContent, "SynSug_WindowTracing", "no")  = "yes" ? True : False
#EndRegion Inline Settings

#Region Validation Check
If Not $bUseSynSug Then
	ConsoleWrite("! > FATAL ERROR: Necessary directive ""#SynSug_Enable=yes"" not found!" & @CRLF)
	Exit
EndIf
#EndRegion Validation Check

#Region Generic Patterns
;Sub Pattern
$spSimpleVar = "\$[a-zA-Z0-9_]+"
$spSimpleIntDec = "[+-]?[0-9]+"
$spSimpleIntHex = "[+-]?0x[0-9a-fA-F]+"
$spSimpleIntSci = "[+-]?[0-9]+e[+-]?[0-9]+"
$spSimpleInt = StringFormat("(?:%s)|(?:%s)|(?:%s)", $spSimpleIntDec, $spSimpleIntHex, $spSimpleIntSci)
$spVarOrInt = StringFormat("(?:%s)|(?:%s)", $spSimpleVar, $spSimpleInt)

Enum $PATTERN, $REPLACE
Local $aspReplacePatterns[11][2]

;Pre- & Postfix-Notationen
$aspReplacePatterns[0][$PATTERN] = "(" & $spSimpleVar & ")\+\+"
$aspReplacePatterns[0][$REPLACE] = "__SYNSUG_POSTINC(\1)"
$aspReplacePatterns[1][$PATTERN] = "\+\+(" & $spSimpleVar & ")"
$aspReplacePatterns[1][$REPLACE] = "__SYNSUG_PREINC(\1)"
$aspReplacePatterns[2][$PATTERN] = "(" & $spSimpleVar & ")\-\-"
$aspReplacePatterns[2][$REPLACE] = "__SYNSUG_POSTDEC(\1)"
$aspReplacePatterns[3][$PATTERN] = "\-\-(" & $spSimpleVar & ")"
$aspReplacePatterns[3][$REPLACE] = "__SYNSUG_PREDEC(\1)"
;Modulo-Operator %
$aspReplacePatterns[4][$PATTERN] = StringFormat("(%s)\h*%%\h*(%s)", $spVarOrInt, $spVarOrInt)
$aspReplacePatterns[4][$REPLACE] = "Mod(\1, \2)"
;BitShift-Operatoren << und >>
$aspReplacePatterns[5][$PATTERN] = StringFormat("(%s)\h*<<\h*(%s)", $spVarOrInt, $spVarOrInt)
$aspReplacePatterns[5][$REPLACE] = "BitShift(\1, -\2)"
$aspReplacePatterns[6][$PATTERN] = StringFormat("(%s)\h*>>\h*(%s)", $spVarOrInt, $spVarOrInt)
$aspReplacePatterns[6][$REPLACE] = "BitShift(\1, \2)"
;BitNot-Operator ~
$aspReplacePatterns[7][$PATTERN] = StringFormat("~(%s)", $spVarOrInt)
$aspReplacePatterns[7][$REPLACE] = "BitNot(\1)"
;C-Style for-Schleife (ausdrucksorientiert)
$aspReplacePatterns[8][$PATTERN] = "(?msi)^\h*for\h*\(\h*(.*?)\h*:\h*(.*?)\h*:\h*(.*?)\h*\)\h*\v(.+?)\v\h*next" ;1:init 2:test 3:cont 4:code
$aspReplacePatterns[8][$REPLACE] = StringFormat("\1\nWhile \2\n\4\n\3\nWEnd")
;Return Inline-1D-Array (Return [a, 42])
$aspReplacePatterns[9][$PATTERN] = "(?i)\h*Return\h*\[(.*)\]"
$aspReplacePatterns[9][$REPLACE] = "Local $__SYNSUG_INLINE_RETURN[] = [\1]" & @CRLF & "Return $__SYNSUG_INLINE_RETURN"
;For ByRef $e In $a
$aspReplacePatterns[10][$PATTERN] = "(?msi)^\h*For\h+(?:ByRef\h+|&)(" & $spSimpleVar & ")\h+In\h+(" & $spSimpleVar & ")\h*\v(.+?)\v\h*Next"
$aspReplacePatterns[10][$REPLACE] = "For $__SYNSUG_ITER_INDEX = 0 To UBound(\2) - 1" & @CRLF & "\1 = \2[$__SYNSUG_ITER_INDEX]" & @CRLF & "\3" & @CRLF & "\2[$__SYNSUG_ITER_INDEX] = \1" & @CRLF & "Next"
;UDF Tracing
If $bUseConsoleTracing Then
	ReDim $aspReplacePatterns[UBound($aspReplacePatterns) + 3][2] ;two more lines!
	$aspReplacePatterns[UBound($aspReplacePatterns) - 3][$PATTERN] = "(?msi)^\h*Func\h+([a-zA-Z0-9_]+)\((.*?)\)(?:.*?)$"
	$aspReplacePatterns[UBound($aspReplacePatterns) - 3][$REPLACE] = "Func \1(\2)" & @CRLF & "Local $__SYNSUG_TRACE = __SynSug_TraceEnterFunc(\1)"
	$aspReplacePatterns[UBound($aspReplacePatterns) - 2][$PATTERN] = "(?msi)Return\h+(.*?)$"
	$aspReplacePatterns[UBound($aspReplacePatterns) - 2][$REPLACE] = "Return __SynSug_TraceLeaveFunc($__SYNSUG_TRACE, \1)"
	$aspReplacePatterns[UBound($aspReplacePatterns) - 1][$PATTERN] = "(?msi)^\h*EndFunc\h*$"
	$aspReplacePatterns[UBound($aspReplacePatterns) - 1][$REPLACE] = "__SynSug_TraceLeaveFunc($__SYNSUG_TRACE, Null)" & @CRLF & "EndFunc"
EndIf
#EndRegion Generic Patterns

#Region Processing
;Extract and process strings
$aStrings = StringMask($sFileContent)
NowDocStrings($sFileContent)
$aStringsND = StringMask($sFileContent, "SYNSUG_NOWDOC_")

If $bUseEscSeq Then
	ConsoleWrite("- Directive > Using escape sequences in string literals." & @CRLF)
	For $i = 0 To UBound($aStrings) - 1
		$sQuotes = StringLeft($aStrings[$i], 1)
		$aStrings[$i] = StringRegExpReplace($aStrings[$i], "(?<!\\)\\n", $sQuotes & " & @LF & " & $sQuotes)
		$aStrings[$i] = StringRegExpReplace($aStrings[$i], "(?<!\\)\\r", $sQuotes & " & @CR & " & $sQuotes)
		$aStrings[$i] = StringRegExpReplace($aStrings[$i], "(?<!\\)\\t", $sQuotes & " & @TAB & " & $sQuotes)
	Next
EndIf
;Remove Comments
$sFileContent = StringRegExpReplace($sFileContent, "(?m);.*?$", "")
;Inline-Structs
$sFileContent = InlineStructs($sFileContent)
;Extended Int Literals (oct and bin)
$sFileContent = IntLiteralsToInt($sFileContent)
;Inline-Dictionaries
$sFileContent = ProcessDictionaries($sFileContent)
;Goto Commands
$sFileContent = ProcessGoto($sFileContent)

For $i = 0 To UBound($aspReplacePatterns) - 1
	$sFileContent = StringRegExpReplace($sFileContent, $aspReplacePatterns[$i][$PATTERN], $aspReplacePatterns[$i][$REPLACE])
Next
If $bUsePrintf Then
	ConsoleWrite("- Directive > Calling printf() is possible." & @CRLF)
	$sFileContent = StringRegExpReplace($sFileContent, "(?i)\h*printf\((.+)\)", " ConsoleWrite(StringFormat(\1))")
EndIf

;THIS IS NECESSARY, need double quotes in nowdoc literals, FAIL: no single quotes allowed....
For $i = 0 To UBound($aStringsND) - 1 ;maybe make this directly in StringUnmask?
	$aStringsND[$i] = StringRegExpReplace($aStringsND[$i], "(@SYNSUG_STR_\d+@)", '"\1"')
Next
StringUnmask($sFileContent, $aStringsND, "SYNSUG_NOWDOC_")
StringUnmask($sFileContent, $aStrings)

$sFileContent &= @CRLF & @CRLF
$sFileContent &= "; SYNSUG HELPER FUNCTIONS" & @CRLF
$sFileContent &= FileRead(@ScriptDir & "\synsughelper.au3")
#EndRegion Processing

FileDelete($sOutFile)
FileWrite($sOutFile, $sFileContent)

;Execute
If $sSwitch = "-e" Then
	;"$(SciteDefaultHome)\AutoIt3Wrapper\AutoIt3Wrapper.exe" /run /prod /ErrorStdOut /in "$(FilePath)"
	$sWorkPath = StringRegExpReplace($sInFile, "(.+)\\.+?$", "\1")
	$sExecuteCmd = StringFormat('"%s\\AutoIt3.exe" "%s\\SciTE\\AutoIt3Wrapper\\AutoIt3Wrapper.au3" /run /prod /ErrorStdOut /in "%s"', $sAu3Path, $sAu3Path, $sOutFile)
	ConsoleWrite("> SynSug: Execute Script... " & $sExecuteCmd & @CRLF)
	$iPID = Run($sExecuteCmd, $sWorkPath, Default, 2)
	While True
        $sOutput = StdoutRead($iPID)
        If @error Then
            ExitLoop
        EndIf
        ConsoleWrite($sOutput)
    WEnd
	ProcessWaitClose($iPID)
	FileDelete($sOutFile)
	Exit
EndIf

;Compile
If $sSwitch = "-c" Then
	;"$(SciteDefaultHome)\AutoIt3Wrapper\AutoIt3Wrapper.exe" /prod /in "$(FilePath)"
	$sCompileCmd = StringFormat('"%s\\AutoIt3.exe" "%s\\SciTE\\AutoIt3Wrapper\\AutoIt3Wrapper.au3" /prod /in "%s" /out "%s.exe"', $sAu3Path, $sAu3Path, $sOutFile, StringTrimRight($sInFile, 4))
	ConsoleWrite("> SynSug: Compiling... " & $sCompileCmd & @CRLF)
	RunWait($sCompileCmd)
	FileDelete($sOutFile)
	Exit
EndIf

;Debug
If $sSwitch = "-d" Then
	ConsoleWrite(StringFormat('The translated script was saved to: "' & $sOutFile & '"' & @CRLF))
	$sCmd = StringFormat('"%s\\SciTE\\tidy\\tidy.exe" "%s"', $sAu3Path, $sOutFile)
	ConsoleWrite("Running Tidy, please wait... " & $sCmd & @CRLF)
	RunWait($sCmd, "", @SW_HIDE)
	ConsoleWrite("Opening script in your editor..." & @CRLF)
	ShellExecute($sOutFile, Default, Default, "edit")
	Exit
EndIf

; ___ FUNCTIONS ___

Func NowDocStrings(ByRef $sCode)
	$aLines = StringSplit($sCode, @CRLF, 3)
	$sNewCode = ""
	$bInNowDoc = False
	$sNowDocDelim = Null
	For $i = 0 To UBound($aLines) - 1
		If StringInStr($aLines[$i], "<<<") And Not $bInNowDoc Then ;beginning
			$aDelim = StringRegExp($aLines[$i], "<<<([a-zA-Z0-9]+)$", 3)
			If Not IsArray($aDelim) Then Exit 42 ;faulty, todo: make it better
			$sNowDocDelim = $aDelim[0]
			$bInNowDoc = True
			$aLines[$i] = StringTrimRight($aLines[$i], StringLen("<<<" & $sNowDocDelim))
			$aLines[$i] &= '"" & _ ;NowDoc Literal'
		ElseIf $aLines[$i] == $sNowDocDelim And $bInNowDoc Then ;end
			$aLines[$i] = '""' ;it's right!
			$bInNowDoc = False
		ElseIf $bInNowDoc Then ;nowdoc line
			$aLines[$i] = '"' & StringReplace($aLines[$i], '"', '""') & '" & @CRLF & _'
		EndIf
		$sNewCode &= $aLines[$i] & @CRLF;reconstruct code string
	Next
	$sCode = $sNewCode
EndFunc

Func StringMask(ByRef $sCode, $sIdentPrefix = "SYNSUG_STR_")
	$spString = "(""|')(.+?)(?!\g1).(\g1)(?!\g1)"
	$aMatches = StringRegExp($sCode, $spString, 4)
	Local $aStrings[UBound($aMatches)]
	For $i = 0 To UBound($aMatches) - 1
		$aStrings[$i] = ($aMatches[$i])[0]
	Next
	For $i = 0 To UBound($aStrings) - 1
		$sCode = StringReplace($sCode, $aStrings[$i], "@" & $sIdentPrefix & $i & "@", 1)
	Next
	Return $aStrings
EndFunc

Func StringUnmask(ByRef $sCode, $aStrings, $sIdentPrefix = "SYNSUG_STR_")
	For $i = 0 To UBound($aStrings) - 1
		$sCode = StringReplace($sCode, "@" & $sIdentPrefix & $i & "@", $aStrings[$i], 1)
	Next
EndFunc

Func IntLiteralsToInt($sCode)
	;Bin
	$aMatches = StringRegExp($sCode, "0b(?:[01]+)", 3)
	For $i = 0 To UBound($aMatches) - 1
		$sCode = StringReplace($sCode, $aMatches[$i], StringToInt(StringTrimLeft($aMatches[$i], 2), 2))
	Next

;~ 	$aMatches = StringRegExp($sCode, "[^0-9a-fA-FxX]0[0-7]+", 3)
;~ 	For $i = 0 To UBound($aMatches) - 1
;~ 		$sCode = StringReplace($sCode, $aMatches[$i], StringLeft($aMatches[$i], 2) & StringToInt(StringTrimLeft($aMatches[$i], 2), 8))
;~ 	Next
	Return $sCode
EndFunc

Func ProcessDictionaries($sFileContent)
	;Inline-Dictionaries (Local $oDict = ["Key" => "Value", 42 => "Sinn"]

	;liste von dicts
	$aDictVars = StringRegExp($sFileContent, StringFormat("(?msi)^\h*(?:Local|Global|Dim)\h+Dict\h+(%s)\h*$", $spSimpleVar), 3)
	If @error Then Local $aDictVars[0]
	$spSearch = StringFormat("(?msi)^\h*(Local|Global|Dim)\h+Dict\h+(%s)\h*$", $spSimpleVar)
	$spReplace = "\1 \2 = ObjCreate('Scripting.Dictionary')"
	$sFileContent = StringRegExpReplace($sFileContent, $spSearch, $spReplace)

	;php-like initialisierungen
	$spSearch = StringFormat("(?msi)^\h*(?:Local|Global|Dim)\h+Dict\h+(?:%s)\h+=\h+\[[^]]+?=>[^]]+?\]", $spSimpleVar)
	$aMatches = StringRegExp($sFileContent, $spSearch, 3)
	If Not @error Then
		For $sMatch In $aMatches
			;\h+\[(?:\h*([^],]+?)\h*=>\h*([^],]+?)\h*,?\h*)+\]
			$spExtractVar = StringFormat("(?msi)^\h*(Local|Global|Dim)\h+Dict\h+(%s)\h+=", $spSimpleVar)
			$spExtractPairs = "(?msi)(?:\[|,)\h*([^],]+)\h*=>\h*([^],]+)"
			$aVarInfo = StringRegExp($sMatch, $spExtractVar, 3)
			$aPairs = StringRegExp($sMatch, $spExtractPairs, 3)
			_ArrayAdd($aDictVars, $aVarInfo[1])

			$sReplace = $aVarInfo[0] & " " & $aVarInfo[1] & " = ObjCreate('Scripting.Dictionary')" & @CRLF
			For $i = 0 To UBound($aPairs) - 1 Step 2
				$sReplace &= $aVarInfo[1] & "(" & $aPairs[$i] & ") = " & $aPairs[$i + 1] & @CRLF
			Next
			$sFileContent = StringReplace($sFileContent, $sMatch, $sReplace)
		Next
	EndIf

	;kurzschreibweisen ([]-Zugriff, Ubound)
	For $sVar In $aDictVars
		$sFileContent = StringRegExpReplace($sFileContent, "(?msi)\Q" & $sVar & "\E\[(.+?)\]", $sVar & "(\1)")
		$sFileContent = StringRegExpReplace($sFileContent, "(?msi)UBound\h*\(\h*\Q" & $sVar & "\E\h*\)", $sVar & ".count")
	Next

	Return $sFileContent
EndFunc

Func ProcessGoto($sFileContent)
	Return $sFileContent
EndFunc

Func InlineStructs($sFileContent)
	;alle strukturdefinitionen finden ...
	$aStructDefsRaw = StringRegExp($sFileContent, "(?msi)^\h*BeginStruct\h+([0-9a-zA-Z_]+)\h*$(.+?)^\h*EndStruct\h*$", 3)
	If Not IsArray($aStructDefsRaw) Then Return $sFileContent
	$oStructDefs = ObjCreate("Scripting.Dictionary")
	For $i = 0 To UBound($aStructDefsRaw) - 1 Step 2
		$s = StringReplace($aStructDefsRaw[$i + 1], @CRLF, "; ")
		$s = StringTrimLeft($s, 1) ;dirty...
		$s = StringRegExpReplace($s, "\h+", " ")
		$oStructDefs($aStructDefsRaw[$i + 0]) = $s
	Next
	; ... und anschließend aus dem Skript löschen
	$sFileContent = StringRegExpReplace($sFileContent, "(?msi)^\h*BeginStruct(?:.+?)EndStruct\h*$", "")
	;jetzt alle variableninitialisierungen finden und ersetzen!
	$aStructInitsRaw = StringRegExp($sFileContent, "(?msi)^\h*(Local|Global|Dim)\h+Struct\h+([0-9a-zA-Z_]+)\h+(.+?)$", 4)
	For $aStructInit In $aStructInitsRaw
		$sModifier = $aStructInit[1]
		$sStructDef = $oStructDefs($aStructInit[2])
		$aVarList = StringRegExp($aStructInit[3], StringFormat("(%s)", $spSimpleVar), 3)
		$sReplace = ""
		For $sVar In $aVarList
			$sReplace &= StringFormat("%s %s = DllStructCreate(""%s"")\n", $sModifier, $sVar, $sStructDef)
		Next
		$sFileContent = StringReplace($sFileContent, $aStructInit[0], $sReplace, 1)
	Next
	Return $sFileContent
EndFunc

Func StringToInt($sStr, $iBase)
	$aRet = DllCall("msvcrt.dll", "long:cdecl", "strtol", "str", $sStr, "str*", 0, "int", $iBase)
	If @error Then Return 0
	Return $aRet[0]
EndFunc

Func ReadDirective($sFileContent, $sDirective, $sDefault)
	$aMatches = StringRegExp($sFileContent, "(?msi)^\Q#" & $sDirective & "\E\h*\=\h*(.+?)\h*$", 3)
	If @error Then Return $sDefault
	Return $aMatches[0]
EndFunc