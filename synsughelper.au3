#AutoIt3Wrapper_Run_AU3Check=N
#AutoIt3Wrapper_Run_Tidy=N

Func __SYNSUG_POSTINC(ByRef $int)
	$int += 1
	Return $int - 1
EndFunc

Func __SYNSUG_PREINC(ByRef $int)
	$int += 1
	Return $int
EndFunc

Func __SYNSUG_POSTDEC(ByRef $int)
	$int -= 1
	Return $int + 1
EndFunc

Func __SYNSUG_PREDEC(ByRef $int)
	$int -= 1
	Return $int
EndFunc

Func __SynSug_TraceEnterFunc($vFunc)
	Local $aTrace[2]
	ConsoleWrite("!TRACE")
	For $i = 1 To __SynSug_TraceLevel(0)
		ConsoleWrite("-")
	Next
	ConsoleWrite("> Entered function " & FuncName($vFunc) & "()." & @CRLF)
	__SynSug_TraceLevel(+1)
	$aTrace[0] = $vFunc
	$aTrace[1] = TimerInit()
	Return $aTrace
EndFunc

Func __SynSug_TraceLeaveFunc($aTrace, $vReturn)
	Local $iErr, $iExt
	$iErr = @error
	$iExt = @extended
	__SynSug_TraceLevel(-1)
	ConsoleWrite("!TRACE")
	For $i = 1 To __SynSug_TraceLevel(0)
		ConsoleWrite("-")
	Next
	ConsoleWrite("> Leaved function " & FuncName($aTrace[0]) & "() after " & TimerDiff($aTrace[1]) & " ms." & @CRLF)
	Return SetError($iErr, $iExt, $vReturn)
EndFunc

Func __SynSug_TraceLevel($iChange)
	Local Static $iLevel = 0
	$iLevel += $iChange
	Return $iLevel
EndFunc