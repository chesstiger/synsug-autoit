#AutoIt3Wrapper_Change2CUI=y
;#RequireAdmin

printf("- = Syntactic Sugar (c) %s chesstiger = -\n", @YEAR)
printf("- =   Install Script for SciTE4AutoIt3  = -\n\n")
Sleep(500) ;waiting for cmd.exe

$sAutoItDir = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\AutoIt v3\AutoIt", "InstallDir")
printf("Path to AutoIt3: ""%s""\n", $sAutoItDir)
$sUserConfigPath = @LocalAppDataDir & "\AutoIt v3\SciTE\SciTEUser.properties"
printf("Path to SciTEUser.properties: ""%s""\n\n", $sUserConfigPath)

$sSynSugDir = $sAutoItDir & "\SciTE\SynSug"

;is synsug.exe existing?
If Not FileExists("synsug.exe") Then
	printf("Warning! No compiled version of synsug (file 'synsug.exe') found!\n")
	printf("We'll try to compile it... Please wait.\n")
	$sCmd = StringFormat('"%s\AutoIt3.exe" "%s\SciTE\AutoIt3Wrapper\AutoIt3Wrapper.au3" /prod /in "synsug.au3" /out "synsug.exe"', $sAutoItDir, $sAutoItDir)
	$iExit = RunWait($sCmd)
	printf("Au3Wrapper Exit Code: %d\n\n", $iExit)
	If $iExit Then
		printf("Sorry! Something went wrong. Cannot compile SynSug. Try to do this manually.\n")
		QuitWait()
	EndIf
EndIf

;move files to target directory
DirCreate($sSynSugDir)
FileCopy("synsug.exe", $sSynSugDir & "\synsug.exe", 1)
$bSuccess = FileCopy("synsughelper.au3", $sSynSugDir & "\synsughelper.au3", 1)
If Not $bSuccess Then
	printf("Error! Cannot copy files! Maybe you have to start this script with administrative rights?\n")
	QuitWait()
EndIf
printf("Files moved successfuly.\n")

;edit config file
$sContent = FileRead($sUserConfigPath)
If StringInStr($sContent, "#Begin Syntactic Sugar") Then
	printf("SciTEUser.properties was already touched by this script. No changes.\n")
Else
	printf("Making backup of SciTEUser.properties as ""SciTEUser.properties.backup_synsug""...\n")
	If Not FileExists($sUserConfigPath & ".backup_synsug") Then FileCopy($sUserConfigPath, $sUserConfigPath & ".backup_synsug")
	printf("Searching for free command numbers in SciTEUser.properties...\n")
	$sContentAdd = @CRLF & "#Begin Syntactic Sugar" & @CRLF
	$iNum = 38 ;start num
	While True ;execute
		If StringRegExp($sContent, "(?msi)^\h*\Qcommand.name." & $iNum & "\E") Then
			$iNum += 1
			ContinueLoop
		EndIf
		printf("\t%d is free. Now it's used for %s.\n", $iNum, "Syntactic Sugar (Execute)")
		$sContentAdd &= StringFormat("command.name.%d.$(au3)=Syntactic Sugar (Execute)", $iNum) & @CRLF
		$sContentAdd &= StringFormat("command.%d.*.au3=""%s\synsug.exe"" -e ""$(autoit3dir)"" ""$(FilePath)""", $iNum, $sSynSugDir) & @CRLF
		$sContentAdd &= StringFormat("command.mode.%d.*=savebefore:yes", $iNum) & @CRLF
		$sContentAdd &= StringFormat("command.shortcut.%d.*.au3=Ctrl+Shift+F5", $iNum) & @CRLF
		$iNum += 1
		ExitLoop
	WEnd
	While True ;compile
		If StringRegExp($sContent, "(?msi)^\h*\Qcommand.name." & $iNum & "\E") Then
			$iNum += 1
			ContinueLoop
		EndIf
		printf("\t%d is free. Now it's used for %s.\n", $iNum, "Syntactic Sugar (Compile)")
		$sContentAdd &= StringFormat("command.name.%d.$(au3)=Syntactic Sugar (Compile)", $iNum) & @CRLF
		$sContentAdd &= StringFormat("command.%d.*.au3=""%s\synsug.exe"" -c ""$(autoit3dir)"" ""$(FilePath)""", $iNum, $sSynSugDir) & @CRLF
		$sContentAdd &= StringFormat("command.mode.%d.*=savebefore:yes", $iNum) & @CRLF
		$sContentAdd &= StringFormat("command.shortcut.%d.*.au3=Ctrl+Shift+F7", $iNum) & @CRLF
		$iNum += 1
		ExitLoop
	WEnd
	While True ;debug to file
		If StringRegExp($sContent, "(?msi)^\h*\Qcommand.name." & $iNum & "\E") Then
			$iNum += 1
			ContinueLoop
		EndIf
		printf("\t%d is free. Now it's used for %s.\n", $iNum, "Syntactic Sugar (Debug to File)")
		$sContentAdd &= StringFormat("command.name.%d.$(au3)=Syntactic Sugar (Debug to File)", $iNum) & @CRLF
		$sContentAdd &= StringFormat("command.%d.*.au3=""%s\synsug.exe"" -d ""$(autoit3dir)"" ""$(FilePath)""", $iNum, $sSynSugDir) & @CRLF
		$sContentAdd &= StringFormat("command.mode.%d.*=savebefore:yes", $iNum) & @CRLF
		ExitLoop
	WEnd
	$sContentAdd &= "#End Syntactic Sugar" & @CRLF
	printf("Writing changes back to SciTEUser.properties...\n")
	FileDelete($sUserConfigPath)
	FileWrite($sUserConfigPath, $sContent & $sContentAdd)
EndIf

printf("\nOk, that was it. It's done, Syntactic Sugar for AutoIt is installed. :)\n")

QuitWait()

Func QuitWait()
	printf("\nScript ended. Use close button or press Ctrl+C to terminate it.")
	While True
		Sleep(25)
	WEnd
EndFunc


Func printf($sIn, $v1 = 0, $v2 = 0, $v3 = 0, $v4 = 0)
	Local $sOut
	Switch @NumParams
		Case 1
			$sOut = StringFormat($sIn)
		Case 2
			$sOut = StringFormat($sIn, $v1)
		Case 3
			$sOut = StringFormat($sIn, $v1, $v2)
		Case 4
			$sOut = StringFormat($sIn, $v1, $v2, $v3)
		Case 5
			$sOut = StringFormat($sIn, $v1, $v2, $v3, $v4)
	EndSwitch
	ConsoleWrite($sOut)
EndFunc